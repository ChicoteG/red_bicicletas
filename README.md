# Semana 1 del proyecto "red de bicicletas" del curso de programación backend de la Universidad Austral.

## Para ver el proyeto:
* Descargar el repositorio:
`$ git clone https://ChicoteG@bitbucket.org/ChicoteG/red_bicicletas.git`

* Ir al directorio del proyecto:
`$ cd red_bicicletas`

* Instalar los módulos necesarios con el comando
`$ npm install`

* Correr el servidor con el comando
`$ npm run devstart`

* La visualización del proyecto estará visible en el endpoint "localhost:3000"