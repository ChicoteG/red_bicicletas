var Bicicleta = function (id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function () {
    return 'id: ' + this.id + ' | color: ' + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function (aBici) {
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(biciId) {
    var bici = Bicicleta.allBicis.find( x => x.id == biciId );
    if (bici) return bici;
    else throw new Error(`No existe una bici con id ${biciId}`);
}

Bicicleta.removeById = function(biciId) {
    for (var i = 0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == biciId) {
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
    /*
    if (i == Bicicleta.allBicis.length)
        throw new Error(`No existe una bici con id ${biciId}`);
    */
}

var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
var b = new Bicicleta(2, 'blanco', 'urbana', [-34.596932, -58.3808287]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;