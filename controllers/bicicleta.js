var Bicicleta = require('../models/bicicleta');

// Listado de bicicletas
exports.bicicleta_list = (req, res) => {
    res.render('bicicletas/index', { bicis: Bicicleta.allBicis })
}

// Formulario de creación de bicicletas
exports.bicicleta_create_get = (req, res) => {
    res.render('bicicletas/create');
}

// Post request de creación
exports.bicicleta_create_post = (req, res) => {
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);

    res.redirect('/bicicletas');
}

// Formulario de modificación de bicicletas
exports.bicicleta_update_get = (req, res) => {
    let bici = Bicicleta.findById(req.params.id);
    res.render('bicicletas/update', { bici } );
}

// Post request de modificación
exports.bicicleta_update_post = (req, res) => {
    let bici = Bicicleta.findById(req.params.id);
    console.log('El id es: ' + req.body.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];

    res.redirect('/bicicletas');
}

// Post request de eliminación
exports.bicicleta_delete_post = (req, res) => {
    Bicicleta.removeById(req.body.id);

    res.redirect('/bicicletas');
}
